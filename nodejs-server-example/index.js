const express = require('express');
const app = express();
const PORT = 8080;

app.listen(PORT, () =>
  console.log(`Server is running on http://localhost:${PORT}`),
);

app.use(express.json());

app.get('/tshirt', (req, res) => {
  res.status(200).send({
    tshirt: 'Custom logo designed T-Shirt',
    size: 'large',
  });
});

app.post('/tshirt/:id', (req, res) => {
  const {id} = req.params;
  const {logo} = req.body;

  console.log('req body ---> ', req.body);

  if (!logo) {
    res.status(418).send({message: 'We need a logo!'});
  }

  res.send({
    tshirt: `T-shirt with your ${logo} and ID of ${id} is ready!`,
  });
});
