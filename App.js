import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen';
import AddTwoNumbers from './screens/AddTwoNumbers';
import DigitalClock from './screens/DigitalClock';
import CameraScreen from './screens/CameraScreen';
import GoogleMapsScreen from './screens/GoogleMapsScreen';
import GalleryScreen from './screens/GalleryScreen';
import showImage from './src/showImage';
import Counter from './screens/Counter';
import CountryPickerScreen from './screens/CountryPickerScreen';
import MultiplicationTable from './screens/MultiplicationTable';
import FetchDataFromAPI from './screens/FetchDataFromAPI';
import ExampleAPIDemo from './screens/ExampleAPIDemo';
import SoundDemo from './screens/SoundDemo';
import useStateExample from './screens/useStateExample';
import useReducerExample from './screens/useReducerExample';
import useEffectExample from './screens/useEffectExample';
import useRefExample from './screens/useRefExample';
import useImperativeHandleExample from './screens/useImperativeHandleExample';
import useContextExample from './screens/useContextExample';
import useCallbackExample from './screens/useCallbackExample';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{headerTitle: 'Home Screen'}}
        />
        <Stack.Screen
          name="AddTwoNumbers"
          component={AddTwoNumbers}
          options={{headerTitle: 'Add Two Numbers'}}
        />
        <Stack.Screen
          name="FetchDataFromAPI"
          component={FetchDataFromAPI}
          options={{headerTitle: 'Fetch Data from API Example'}}
        />
        <Stack.Screen
          name="ExampleAPIDemo"
          component={ExampleAPIDemo}
          options={{headerTitle: 'Example REST API Demo'}}
        />
        <Stack.Screen
          name="DigitalClock"
          component={DigitalClock}
          options={{headerTitle: 'Digital Clock'}}
        />
        <Stack.Screen
          name="CameraScreen"
          component={CameraScreen}
          options={{headerTitle: 'Camera Screen'}}
        />
        <Stack.Screen
          name="GoogleMapsScreen"
          component={GoogleMapsScreen}
          options={{headerTitle: 'Google Maps Screen'}}
        />
        <Stack.Screen
          name="GalleryScreen"
          component={GalleryScreen}
          options={{headerTitle: 'Gallery'}}
        />
        <Stack.Screen
          name="showImage"
          component={showImage}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Counter" component={Counter} />
        <Stack.Screen
          name="CountryPicker"
          component={CountryPickerScreen}
          options={{headerTitle: 'Country Picker Screen'}}
        />
        <Stack.Screen
          name="MultiplicationTable"
          component={MultiplicationTable}
          options={{headerTitle: 'Multiplication Table Demo'}}
        />
        <Stack.Screen
          name="SoundDemo"
          component={SoundDemo}
          options={{headerTitle: 'Play Sound Demo'}}
        />
        <Stack.Screen
          name="useStateExample"
          component={useStateExample}
          options={{headerTitle: 'useState Hook Example'}}
        />
        <Stack.Screen
          name="useReducerExample"
          component={useReducerExample}
          options={{headerTitle: 'useReducer Hook Example'}}
        />
        <Stack.Screen
          name="useEffectExample"
          component={useEffectExample}
          options={{headerTitle: 'useEffect Hook Example'}}
        />
        <Stack.Screen
          name="useRefExample"
          component={useRefExample}
          options={{headerTitle: 'useRef Hook Example'}}
        />
        <Stack.Screen
          name="useImperativeHandleExample"
          component={useImperativeHandleExample}
          options={{headerTitle: 'useImperativeHandle Hook'}}
        />
        <Stack.Screen
          name="useContextExample"
          component={useContextExample}
          options={{headerTitle: 'useContext Hook Example'}}
        />
        <Stack.Screen
          name="useCallbackExample"
          component={useCallbackExample}
          options={{headerTitle: 'useCallback Hook Example'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
