export default Images = [
  {url: require('../assets/image1.jpg')},
  {url: require('../assets/image2.png')},
  {url: require('../assets/image3.jpeg')},
  {url: require('../assets/image4.jpg')},
  {url: require('../assets/image5.png')},
  {url: require('../assets/image6.jpg')},
];
