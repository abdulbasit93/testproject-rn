import React, {useContext} from 'react';
import {View, Text} from 'react-native';
import {AppContext} from '../../screens/useContextExample';

function User() {
  const {username} = useContext(AppContext);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 30, fontWeight: 'bold', textAlign: 'center'}}>
        User:
      </Text>
      <View
        style={{
          borderWidth: 1,
          borderColor: 'red',
          width: 330,
          justifyContent: 'center',
          padding: 20,
        }}>
        <Text
          style={{
            fontSize: 25,
            textAlign: 'center',
          }}>
          {username}
        </Text>
      </View>
    </View>
  );
}

export default User;
