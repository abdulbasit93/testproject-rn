import React, {useContext} from 'react';
import {View, TextInput} from 'react-native';
import {AppContext} from '../../screens/useContextExample';

function Login() {
  const {setUsername} = useContext(AppContext);

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
      }}>
      <TextInput
        onChangeText={text => setUsername(text)}
        placeholder="Enter username..."
        style={{
          borderColor: 'green',
          borderWidth: 2,
          width: 330,
        }}
      />
    </View>
  );
}

export default Login;
