import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const Button = forwardRef((props, ref) => {
  const [toggle, setToggle] = useState(false);

  useImperativeHandle(ref, () => ({
    alterToggle() {
      setToggle(!toggle);
    },
  }));

  return (
    <View>
      <TouchableOpacity
        style={{backgroundColor: 'green', padding: 20, marginBottom: 15}}>
        <Text style={{color: '#fff', fontSize: 18}}>Button From Child</Text>
      </TouchableOpacity>

      {toggle && (
        <Text style={{fontSize: 26, textAlign: 'center'}}>Toggle</Text>
      )}
    </View>
  );
});

export default Button;
