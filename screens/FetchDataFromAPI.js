import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Touchable,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

const FetchDataFromAPI = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  console.log('data ---> ', data);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => setData(json))
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={{flex: 1, padding: 24}}>
      {isLoading ? (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: 20}}>Loading...</Text>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 25,
              color: '#0e8eeb',
              textAlign: 'center',
              marginBottom: 10,
            }}>
            Users:
          </Text>
          <FlatList
            data={data}
            keyExtractor={({id}, index) => id}
            renderItem={({item}) => (
              <View>
                <Text
                  style={{
                    backgroundColor: '#0e8eeb',
                    margin: 5,
                    padding: 20,
                    color: '#fff',
                    fontSize: 18,
                  }}>
                  Name: {item.name}
                  {'\n'}
                  {'\n'}
                  Email: {item.email}
                  {'\n'}
                  {'\n'}
                  Website: {item.website}
                  {'\n'}
                  {'\n'}
                  Company Name: {item.company.name}
                </Text>
              </View>
            )}
          />
        </View>
      )}
    </View>
  );
};

export default FetchDataFromAPI;
