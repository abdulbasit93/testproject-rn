import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const Counter = () => {
  const [defaultValue, setDefaultValue] = useState(0);

  const increment = () => {
    var newdefaultValue = defaultValue + 1;
    setDefaultValue(newdefaultValue);
  };

  const decrement = () => {
    var decreValue = defaultValue - 1;
    setDefaultValue(decreValue);
  };

  const reset = () => {
    const resetValue = 0;
    setDefaultValue(resetValue);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.numdisplay}>{defaultValue}</Text>
      <View style={styles.btnContainer}>
        <TouchableOpacity style={styles.btnStyle} onPress={() => increment()}>
          <Text style={styles.btnText}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnStyle} onPress={() => decrement()}>
          <Text style={styles.btnText}>-</Text>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity style={styles.btnStyle} onPress={() => reset()}>
          <Text style={styles.btnText}>Reset</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Counter;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  numdisplay: {
    fontSize: 50,
    fontWeight: 'bold',
  },
  btnContainer: {
    flexDirection: 'row',
  },
  btnStyle: {
    backgroundColor: '#007fbe',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 30,
  },
  btnText: {
    color: '#fff',
    fontSize: 32,
  },
});
