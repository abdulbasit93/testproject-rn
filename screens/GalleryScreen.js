import React from 'react';
import {TouchableOpacity, View, Dimensions, ScrollView} from 'react-native';
import Images from '../src/Images';
import Gallery from 'react-native-image-gallery';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

const GalleryScreen = props => {
  return (
    // <ScrollView>
    //   <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
    //     {Images.map((image, index) => (
    //       <TouchableOpacity
    //         key={index}
    //         onPress={() =>
    //           props.navigation.navigate('showImage', {url: image.url})
    //         }>
    //         <Image
    //           source={image.url}
    //           style={{
    //             height: deviceHeight / 3,
    //             width: deviceWidth / 3 - 6,
    //             borderRadius: 10,
    //             margin: 2,
    //           }}
    //         />
    //       </TouchableOpacity>
    //     ))}
    //   </View>
    // </ScrollView>

    <Gallery
      style={{flex: 1, backgroundColor: 'black'}}
      images={[
        {
          source: require('../assets/image1.jpg'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
        {
          source: require('../assets/image2.png'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
        {
          source: require('../assets/image3.jpeg'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
        {
          source: require('../assets/image4.jpg'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
        {
          source: require('../assets/image5.png'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
        {
          source: require('../assets/image6.jpg'),
          dimensions: {width: deviceWidth, height: deviceHeight},
        },
      ]}
    />
  );
};

export default GalleryScreen;
