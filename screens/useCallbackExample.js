import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';

export default function useCallbackExample() {
  const [number, setNumber] = useState(1);
  const [items, setItems] = useState([]);
  const [dark, setDark] = useState(false);

  const getItems = useCallback(() => {
    return [number, number + 1, number + 2];
  }, [number]);

  useEffect(() => {
    setItems(getItems());
    console.log('Updating Items');
  }, [getItems]);

  const theme = {
    backgroundColor: dark ? '#333' : '#FFF',
    color: dark ? '#FFF' : '#333',
  };

  return (
    <View style={theme}>
      <TextInput
        style={{
          margin: 15,
          borderColor: dark ? '#fff' : '#333',
          borderWidth: 2,
          color: dark ? '#FFF' : '#333',
        }}
        placeholder="Enter number..."
        keyboardType="numeric"
        value={number.toString()}
        onChangeText={text => setNumber(parseInt(text))}
      />
      <View
        style={{justifyContent: 'center', alignItems: 'center', margin: 10}}>
        <TouchableOpacity
          onPress={() => setDark(prevDark => !prevDark)}
          style={{backgroundColor: '#066bbf', padding: 20, width: 160}}>
          <Text style={{color: '#fff', fontSize: 18, textAlign: 'center'}}>
            Toggle Theme
          </Text>
        </TouchableOpacity>
      </View>

      {items.map((item, id) => (
        <View key={id} style={{marginLeft: 18, marginBottom: 10, padding: 5}}>
          <Text style={{fontSize: 25, color: dark ? '#fff' : '#000000'}}>
            {item}
          </Text>
        </View>
      ))}
    </View>
  );
}
