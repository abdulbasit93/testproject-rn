import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';

const DigitalClock = () => {
  const monthsArray = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const [currentTime, setCurrentTime] = useState(null);
  const [currentDay, setCurrentDay] = useState(null);
  const [currentMonth, setCurrentMonth] = useState(
    monthsArray[new Date().getMonth()].toUpperCase(),
  );
  const daysArray = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];

  const getCurrentTime = () => {
    let hour = new Date().getHours();
    let minutes = new Date().getMinutes();
    let seconds = new Date().getSeconds();
    let am_pm = 'pm';

    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    if (seconds < 10) {
      seconds = '0' + seconds;
    }

    if (hour > 12) {
      hour = hour - 12;
    }

    if (hour == 0) {
      hour = 12;
    }

    if (new Date().getHours() < 12) {
      am_pm = 'am';
    }

    setCurrentTime(hour + ':' + minutes + ':' + seconds + ' ' + am_pm);

    monthsArray.map((item, key) => {
      if (key == new Date().getMonth()) {
        setCurrentMonth(item.toUpperCase());
      }
    });

    daysArray.map((item, key) => {
      let date = new Date().getDate();
      let year = new Date().getFullYear();
      if (key == new Date().getDay()) {
        setCurrentDay(
          item.toUpperCase() + ',\n' + date + ' ' + currentMonth + ' ' + year,
        );
      }
    });
  };

  useEffect(() => {
    // componentwillmount
    timer = setInterval(() => {
      getCurrentTime();
    }, 1000);
    return () => {
      // componentwillunmount
      clearInterval(timer);
    };
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.daysText}>{currentDay}</Text>
        <Text style={styles.timeText}>{currentTime}</Text>
      </View>
    </View>
  );
};

export default DigitalClock;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  timeText: {
    fontSize: 50,
    color: '#f44336',
  },
  daysText: {
    color: '#2196f3',
    fontSize: 25,
    paddingBottom: 0,
  },
});
