import React, {useState} from 'react';
import {View, Text, Button} from 'react-native';

const useStateExample = () => {
  const [counter, setCounter] = useState(0);

  const increment = () => {
    setCounter(counter + 1);
  };
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 35, marginBottom: 10, textAlign: 'center'}}>
        {counter}
      </Text>
      <Button title="Increment" onPress={increment} />
    </View>
  );
};

export default useStateExample;
