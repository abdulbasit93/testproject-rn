import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {useCamera} from 'react-native-camera-hooks';
import RNFS from 'react-native-fs';
import Snackbar from 'react-native-snackbar';

export default function CameraScreen() {
  const [{cameraRef}, {takePicture}] = useCamera(null);

  const [cameraSwitch, setCameraSwitch] = useState(
    RNCamera.Constants.Type.back,
  );

  const toggleCameraSwitch = () => {
    if (cameraSwitch == RNCamera.Constants.Type.back) {
      setCameraSwitch(RNCamera.Constants.Type.front);
    } else if (cameraSwitch == RNCamera.Constants.Type.front) {
      setCameraSwitch(RNCamera.Constants.Type.back);
    }
  };

  const captureHandle = async () => {
    try {
      const data = await takePicture();
      console.log(data.uri);
      const filePath = data.uri;
      const newFilePath = `${
        RNFS.ExternalDirectoryPath
      }/${new Date().toISOString()}.jpg`.replace(/:/g, '-');
      RNFS.moveFile(filePath, newFilePath)
        .then(() => {
          console.log('IMAGE MOVED', filePath, '-- to --', newFilePath);
          Snackbar.show({
            text: `Image Moved to: ${newFilePath}`,
            duration: Snackbar.LENGTH_LONG,
          });
        })
        .catch(error => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.body}>
      <RNCamera ref={cameraRef} type={cameraSwitch} style={styles.preview}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={{
              backgroundColor: '#1eb900',
              padding: 10,
              margin: 10,
              width: 120,
            }}
            onPress={() => captureHandle()}>
            <Text style={{color: '#fff', textAlign: 'center'}}>Capture</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: '#1eb900',
              padding: 10,
              margin: 10,
              width: 120,
            }}
            onPress={() => toggleCameraSwitch()}>
            <Text style={{color: '#fff', textAlign: 'center'}}>
              Switch Camera
            </Text>
          </TouchableOpacity>
        </View>
      </RNCamera>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  preview: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
