import React, {useState, createContext} from 'react';
import {View, Text} from 'react-native';
import Login from '../src/Components/Login';
import User from '../src/Components/User';

export const AppContext = createContext(null);

const useContextExample = () => {
  const [username, setUsername] = useState('');

  return (
    <AppContext.Provider value={{username, setUsername}}>
      <Text
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          margin: 30,
        }}>
        <Login /> <User />
      </Text>
    </AppContext.Provider>
  );
};

export default useContextExample;
