import React, {useEffect, useState} from 'react';
import {View, Text, Button} from 'react-native';
import axios from 'axios';

const useEffectExample = () => {
  const [data, setData] = useState('');
  const [count, setCount] = useState(0);

  useEffect(() => {
    axios
      .get('https://jsonplaceholder.typicode.com/comments')
      .then(response => {
        setData(response.data[0].email);
        console.log('API WAS CALLED');
      });
  }, [count]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 25}}>{data}</Text>
      <Text style={{fontSize: 25}}>{count}</Text>
      <Button
        title="Click"
        onPress={() => {
          setCount(count + 1);
        }}
      />
    </View>
  );
};

export default useEffectExample;
