import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import MapView from 'react-native-maps';

const GoogleMapsScreen = () => {
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 24.8572235083438,
          longitude: 66.95839467158507,
          latitudeDelta: 0.2922,
          longitudeDelta: 0.2421,
        }}
        // 24.8572235083438, 66.95839467158507
      />
    </View>
  );
};

export default GoogleMapsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
