import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ActivityIndicator} from 'react-native';

const ExampleAPIDemo = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://172.16.203.168:8080/tshirt')
      .then(response => response.json())
      .then(json => setData(json))
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: 20}}>Loading...</Text>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <>
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>T-Shirt type:</Text>
          <Text style={{fontSize: 20, marginBottom: 30}}>{data.tshirt}</Text>
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>Size:</Text>
          <Text style={{fontSize: 20}}>{data.size}</Text>
        </>
      )}
    </View>
  );
};

export default ExampleAPIDemo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
