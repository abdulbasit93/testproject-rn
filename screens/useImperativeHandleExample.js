import React, {useRef} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Button from '../src/Components/Button';

const useImperativeHandleExample = () => {
  const buttonRef = useRef(null);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity
        onPress={() => {
          buttonRef.current.alterToggle();
        }}
        style={{backgroundColor: 'green', padding: 20, marginBottom: 10}}>
        <Text style={{color: '#fff', fontSize: 18}}>Button From Parent</Text>
      </TouchableOpacity>

      <Button ref={buttonRef} />
    </View>
  );
};

export default useImperativeHandleExample;
