import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import dings from '../assets/win-vista.mp3';
import dings2 from '../assets/win-xp.mp3';

var Sound = require('react-native-sound');

Sound.setCategory('Playback');

var ding = new Sound(dings, error => {
  if (error) {
    console.log('Failed to load the sound ', error);
    return;
  }
  // if loaded success
  console.log(
    'duration in seconds: ' +
      ding.getDuration() +
      'number of channels: ' +
      ding.getNumberOfChannels(),
  );
});

var ding2 = new Sound(dings2, error => {
  if (error) {
    console.log('Failed to load the sound ', error);
    return;
  }
  // if loaded success
  console.log(
    'duration in seconds: ' +
      ding2.getDuration() +
      'number of channels: ' +
      ding2.getNumberOfChannels(),
  );
});

const SoundDemo = () => {
  useEffect(() => {
    ding.setVolume(1);
    ding2.setVolume(1);
    return () => {
      ding.release();
      ding2.release();
    };
  }, []);

  const playXPSound = () => {
    ding2.play(success => {
      if (success) {
        console.log('Successfully finished playing');
      } else {
        console.log('Playback failed due to audio decoding errors');
      }
    });
  };

  const playVistaSound = () => {
    ding.play(success => {
      if (success) {
        console.log('Successfully finished playing');
      } else {
        console.log('Playback failed due to audio decoding errors');
      }
    });
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={playXPSound} style={styles.playBtn}>
        <Text style={{color: '#fff'}}>Play Windows XP Startup Sound</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={playVistaSound} style={styles.playBtn}>
        <Text style={{color: '#fff'}}>Play Windows Vista Startup Sound</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SoundDemo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  playBtn: {
    backgroundColor: 'blue',
    padding: 20,
    margin: 10,
  },
});
