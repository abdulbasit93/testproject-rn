import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';

const CountryPickerScreen = () => {
  const [country, setCountry] = useState(null);
  const [countryCode, setcountryCode] = useState('PK');
  const [callingCode, setCallingCode] = useState('92');

  return (
    <View style={styles.container}>
      <Text style={{fontSize: 20, marginBottom: 20}}>
        Tap button below to select country
      </Text>
      <CountryPicker
        withFilter
        countryCode={countryCode}
        withFlag
        withAlphaFilter={false}
        withCurrencyButton={false}
        withCountryNameButton
        withCallingCode
        onSelect={country => {
          console.log('country: ', country);
          setCountry(country);
          const {cca2, callingCode} = country;
          setcountryCode(cca2);
          setCallingCode(callingCode[0]);
        }}
        containerButtonStyle={{
          alignItems: 'center',
          borderWidth: 3,
          borderColor: '#00ceff',
          borderRadius: 30,
          padding: 10,
        }}
      />
      <View style={styles.infoBox}>
        <Text style={{fontSize: 17, fontWeight: 'bold', marginBottom: 10}}>
          Country data:
        </Text>
        {country !== null && (
          <View>
            <Text style={{color: 'black', fontSize: 17}}>
              Name: {JSON.stringify(country.name).replace(/\"/g, '')}
            </Text>
            <Text style={{color: 'black', fontSize: 17}}>
              Region: {JSON.stringify(country.region).replace(/\"/g, '')}
            </Text>
            <Text style={{color: 'black', fontSize: 17}}>
              Subregion: {JSON.stringify(country.subregion).replace(/\"/g, '')}
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default CountryPickerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoBox: {
    backgroundColor: '#C0C0C0',
    margin: 40,
    padding: 40,
  },
});
