import React, {useState} from 'react';

import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const AddTwoNumbers = () => {
  const [num_1, setNum_1] = useState('');
  const [num_2, setNum_2] = useState('');
  const [result, setResult] = useState('');
  //   const [isVisible, setIsVisible] = useState(false);

  const sumofNumbers = () => {
    const firstNum = parseInt(num_1);
    const secondNum = parseInt(num_2);

    const sumResult = firstNum + secondNum;

    if (num_1 == null && num_2 == null) {
      alert('Please enter some values!');
      return;
    } else {
      console.log('The sum is: ', sumResult);
      setResult(sumResult);
      alert('The sum of ' + num_1 + ' and ' + num_2 + ' is: ' + sumResult);
      //   setIsVisible(true);
    }
  };

  const clearInputs = () => {
    setNum_1();
    setNum_2();
    setResult();
    // setIsVisible(false);
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Enter first number"
        style={styles.inputBox}
        keyboardType="numeric"
        value={num_1}
        onChangeText={num_1 => setNum_1(num_1)}
      />
      <TextInput
        placeholder="Enter second number"
        style={styles.inputBox}
        keyboardType="numeric"
        value={num_2}
        onChangeText={num_2 => setNum_2(num_2)}
      />

      <TouchableOpacity
        onPress={() => sumofNumbers()}
        style={{
          backgroundColor: 'green',
          paddingLeft: 30,
          paddingRight: 30,
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        <Text style={{color: '#fff', fontSize: 20}}>Sum</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => clearInputs()}
        style={{
          backgroundColor: 'orangered',
          paddingLeft: 30,
          paddingRight: 30,
          paddingTop: 10,
          paddingBottom: 10,
          marginTop: 20,
        }}>
        <Text style={{color: '#fff', fontSize: 20}}>Clear</Text>
      </TouchableOpacity>

      {/* {isVisible ? (
        <View style={{backgroundColor: 'blue', padding: 20, marginTop: 20}}>
          <Text style={{color: '#fff'}}>
            The sum of {num_1} and {num_2} is: {result}
          </Text>
        </View>
      ) : null} */}
    </View>
  );
};

export default AddTwoNumbers;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    borderWidth: 2,
    borderColor: 'black',
    width: 240,
    padding: 10,
    margin: 10,
  },
});
