import React, {useRef, useState} from 'react';
import {View, Text, TextInput, Pressable} from 'react-native';

const useRefExample = () => {
  const inputRef = useRef(null);
  const [input, setInput] = useState('');
  const pressed = () => {
    inputRef.current.clear();
    // console.log(inputRef.current.textValue);
  };

  return (
    <View>
      <Text style={{fontSize: 25, marginBottom: 15}}>Name</Text>
      <TextInput
        placeholder="Ex..."
        style={{borderColor: 'black', borderWidth: 1, marginBottom: 15}}
        ref={inputRef}
        value={input}
        onChangeText={text => setInput(text)}
      />
      <Pressable
        onPress={pressed}
        style={{backgroundColor: 'blue', padding: 20, margin: 10, width: 110}}>
        <Text style={{color: '#fff', fontSize: 19}}>Clear it!</Text>
      </Pressable>
    </View>
  );
};

export default useRefExample;
