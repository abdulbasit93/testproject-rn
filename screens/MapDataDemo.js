import React from 'react';
import {View, Text, ScrollView} from 'react-native';

const MapDataDemo = () => {
  const array = [
    {
      key: '1',
      title: 'example title 1',
      subtitle: 'example subtitle 1',
    },
    {
      key: '2',
      title: 'example title 2',
      subtitle: 'example subtitle 2',
    },
    {
      key: '3',
      title: 'example title 3',
      subtitle: 'example subtitle 3',
    },
    {
      key: '4',
      title: 'example title 4',
      subtitle: 'example subtitle 4',
    },
    {
      key: '5',
      title: 'example title 5',
      subtitle: 'example subtitle 5',
    },
    {
      key: '6',
      title: 'example title 6',
      subtitle: 'example subtitle 6',
    },
    {
      key: '7',
      title: 'example title 7',
      subtitle: 'example subtitle 7',
    },
    {
      key: '8',
      title: 'example title 8',
      subtitle: 'example subtitle 8',
    },
    {
      key: '9',
      title: 'example title 9',
      subtitle: 'example subtitle 9',
    },
    {
      key: '10',
      title: 'example title 10',
      subtitle: 'example subtitle 10',
    },
  ];

  const list = () => {
    return array.map(element => {
      return (
        <View
          key={element.key}
          style={{margin: 10, padding: 10, backgroundColor: '#43f2eb'}}>
          <Text style={{fontSize: 20}}>{element.title}</Text>
          <Text style={{fontSize: 13}}>{element.subtitle}</Text>
        </View>
      );
    });
  };

  return <ScrollView>{list()}</ScrollView>;
};

export default MapDataDemo;
