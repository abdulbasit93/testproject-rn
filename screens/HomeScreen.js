import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <ScrollView>
      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('CameraScreen')}>
        <Text style={styles.menuTextStyle}>Camera Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('GoogleMapsScreen')}>
        <Text style={styles.menuTextStyle}>Google Maps Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('GalleryScreen')}>
        <Text style={styles.menuTextStyle}>Image Gallery Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('Counter')}>
        <Text style={styles.menuTextStyle}>Counter Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('MultiplicationTable')}>
        <Text style={styles.menuTextStyle}>
          Multiplication Table Demo Screen
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('CountryPicker')}>
        <Text style={styles.menuTextStyle}>Country Picker Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('FetchDataFromAPI')}>
        <Text style={styles.menuTextStyle}>Fetch Data from API Demo</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('ExampleAPIDemo')}>
        <Text style={styles.menuTextStyle}>Example API Demo</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('SoundDemo')}>
        <Text style={styles.menuTextStyle}>Play Sound Demo</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('DigitalClock')}>
        <Text style={styles.menuTextStyle}>Digital Clock Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('AddTwoNumbers')}>
        <Text style={styles.menuTextStyle}>Add Two Numbers Screen</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useStateExample')}>
        <Text style={styles.menuTextStyle}>useState Hook Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useReducerExample')}>
        <Text style={styles.menuTextStyle}>useReducer Hook Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useEffectExample')}>
        <Text style={styles.menuTextStyle}>useEffect Hook Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useRefExample')}>
        <Text style={styles.menuTextStyle}>useRef Hook Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useImperativeHandleExample')}>
        <Text style={styles.menuTextStyle}>
          useImperativeHandle Hook Example
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useContextExample')}>
        <Text style={styles.menuTextStyle}>useContext Hook Example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menuItemStyle}
        onPress={() => navigation.navigate('useCallbackExample')}>
        <Text style={styles.menuTextStyle}>useCallback Hook Example</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  menuTextStyle: {
    fontSize: 20,
    color: '#fff',
  },
  menuItemStyle: {
    padding: 15,
    margin: 10,
    backgroundColor: '#21B57A',
  },
});
