import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, Platform} from 'react-native';

const MultiplicationTable = () => {
  const [num, setNum] = useState(0);
  const indexValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  const onChangedText = text => {
    setNum(text.replace(/\D/g, ''));
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={{
          borderColor: 'black',
          borderWidth: 2,
          width: 210,
          padding: 10,
          marginBottom: 15,
        }}
        keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
        onChangeText={text => onChangedText(text)}
      />
      <Text style={{fontSize: 22}}>Multiplication Table of {num}</Text>
      <View>
        {indexValues.map(index => (
          <Text style={{fontSize: 22}} key={index}>
            {num} x {index} = {index * num}
          </Text>
        ))}
      </View>
    </View>
  );
};

export default MultiplicationTable;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
