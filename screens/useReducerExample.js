import React, {useReducer} from 'react';
import {View, Text, Button} from 'react-native';

const reducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {count: state.count + 1, showText: state.showText};
    case 'toggleShowText':
      return {count: state.count, showText: !state.showText};
    default:
      return state;
  }
};

const useReducerExample = () => {
  const [state, dispatch] = useReducer(reducer, {count: 0, showText: true});

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 35, marginBottom: 10, textAlign: 'center'}}>
        {state.count}
      </Text>
      <Button
        title="Click Here"
        onPress={() => {
          dispatch({type: 'INCREMENT'});
          dispatch({type: 'toggleShowText'});
        }}
      />

      {state.showText && <Text>This is a text</Text>}
    </View>
  );
};

export default useReducerExample;
